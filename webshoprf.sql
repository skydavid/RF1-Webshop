SET FOREIGN_KEY_CHECKS=0;
TRUNCATE orders;
TRUNCATE addresses;
TRUNCATE categories;
TRUNCATE products;
TRUNCATE users;
SET FOREIGN_KEY_CHECKS=1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `last_name`, `password`, `registration_date`, `last_login_date`, `phone_number`, `rank`, `paid_products`, `status`, `cart`) VALUES
(1, 'admin@webshop.com', 'Donary', 'Clintrump', '$2y$13$igv6FBBJP6.H.BZr4v3ooeFcUtoviKZAF5fS7yXZw3LPic32kZj6K', '2016-11-06 21:24:10', '2016-11-07 01:55:31', '123456789', 1, 0, 1, 'a:1:{i:1;a:5:{s:2:"id";i:15;s:4:"name";s:19:"Ecko Untld Szvetter";s:5:"count";i:1;s:5:"price";d:10684;s:5:"image";s:37:"0daf8836260705f6914f86f3bfc20eb7.jpeg";}}');


--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `parent_category`) VALUES
(1, 'Férfi', '97dac76540db9bde4b33bd70b4284b80.jpeg', NULL),
(2, 'Női', '9de3e90381abf2060eaea4c31316804b.jpeg', NULL),
(4, 'Kiegészítő', '02e537bf6a59943754e969aceafdd44a.jpeg', NULL),
(5, 'Nadrág', '52ac82dc51a56441018106fccaa6154f.jpeg', 1),
(6, 'Nadrág', '500e6f2cf6b2ff3304ee86d2884b581a.jpeg', 2),
(7, 'Póló', '27abb613c4fc67f3ffc82c345f75b1de.jpeg', 1),
(8, 'Póló', '07933c881cbbdee9fc24a7b49a5b6be2.jpeg', 2),
(9, 'Pulóver', '3bdad99eacf217ec72b899cde2e9e43d.jpeg', 1),
(10, 'Kabát', 'b0593be7c215a41d69b1d8daa23908de.jpeg', 1),
(11, 'Cipő', '7a32a8a32ee82c3b653782b740c6da67.jpeg', 1),
(12, 'Pulóver', '0dcb965558ef2dd32438b85cd9424539.jpeg', 2),
(13, 'Kabát', '2e6db3d6a622d25eb7871d18fbf9b6a2.jpeg', 2),
(14, 'Cipő', '66b0cf6b279b31422a45f5e71075856f.jpeg', 2),
(16, 'Farmer', 'c3c7ea6823417479ff8251d540a7d9a2.jpeg', 5),
(17, 'Farmer', 'dd0f10e38d8edacca5c44d3c2d6bf453.jpeg', 6),
(18, 'Melegítő', '5641660f0a6ed492c812f486e7c285e7.jpeg', 5),
(19, 'Rövid nadrág', 'f4f77d967b485886bbbceb1f68b910d0.jpeg', 5),
(20, 'Melegítő', 'a4cd6962901f181b35b6eedd6280b997.jpeg', 6),
(21, 'Rövid nadrág', '9fe305908a6b261de34fffc050d6157c.jpeg', 6),
(22, 'Rövid ujjú', '3380df5424b2b7e29bbea21bdde45465.jpeg', 7),
(23, 'Hosszú ujjú', 'e8496ff3d5f078ffbdba7574c552a299.jpeg', 7),
(24, 'Kapucnis', '6f086935832bf558adc31b37473b7d82.jpeg', 9),
(25, 'Szvetter', '65f21b2db233fa661279f030b9ffbe44.jpeg', 9),
(26, 'Téli kabát', 'd4a59c11ba5e45661a4db7f4759e6f56.jpeg', 10),
(27, 'Bőr kabát', '9581218764c05dbcd658164f4908dbc5.jpeg', 10),
(28, 'Deszkás', '0ff4130810ba9993cf6334fa142c4d65.jpeg', 11),
(29, 'Bakancs', 'fcce2a7723ea37065cffdbf442f4a757.jpeg', 11),
(31, 'Rövid ujjú', '81b11c09365acd85908ab295fffaccef.jpeg', 8),
(32, 'Hosszú ujjú', '9d642cc72dfb38585aa51a1c787d1e60.jpeg', 8),
(33, 'Kapucnis', '2bb9903c9ee9bdb28c8d50584ef9d9dd.jpeg', 12),
(34, 'Szvetter', '7bceb900d8abf082c0bc4ee8d4884b94.jpeg', 12),
(35, 'Téli kabát', 'd51d871ccf6a49d59fb0b48070c29bd6.jpeg', 13),
(36, 'Bőr kabát', '778a6ae804989dd7618f4d572474ae08.jpeg', 13),
(37, 'Deszkás', '685ac17e2569e496670fa0ad2beb54b9.jpeg', 14),
(38, 'Bakancs', '605db7d9654b5defa79659aa0768519c.jpeg', 14);

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `owner_user`, `category`, `name`, `quantity`, `description`, `price`, `discount`, `image`) VALUES
(1, 1, 22, 'Air Jordan Póló', 10, 'Kiváló minőségű Air Jordan póló', 10000, NULL, '09fc9232e2b3ac81cfd7a11ff7ad7222.jpeg'),
(2, 1, 22, 'Amstaff póló', 10, 'Kiváló minőségű Amstaff póló.', 5000, NULL, '3c75c8c398a14e5fb7da496e976dcbaa.jpeg'),
(3, 1, 22, 'Ecko Untld Póló', 5, 'Kiváló minőségű Ecko póló.', 9899, NULL, 'ffd0c43526fc68a788f9e328d2abd421.jpeg'),
(4, 1, 22, 'Ecko Untld Póló Fekete', 3, 'Kiváló minőségű fekete Ecko póló.', 12000, NULL, '25064a8220aafc174ca368149ee3d545.jpeg'),
(5, 1, 22, 'F*ck the police póló', 2, NULL, 5000, NULL, '85d5e0221daddef53488ec71d74beeb2.jpeg'),
(6, 1, 23, 'Reebok hosszú ujjú póló fekete', 3, NULL, 8000, NULL, '75de8c92be45855d82230d442709b0a4.jpeg'),
(7, 1, 23, 'Reebok hosszú ujjú póló fekete piros', 5, NULL, 5000, 10, '083b03a1b1c1158fafa6f9a302126ab5.jpeg'),
(8, 1, 23, 'Hoodboyz hosszú ujjú', 12, 'Color: brown Material: 80% cotton 20% polyester', 2500, NULL, '3c5a089c8999a0fbcd53ad2f42b76f0c.jpeg'),
(9, 1, 23, 'Reebok hosszú ujjú póló szürke', 2, 'Color: grey Material: 80% cotton 20% polyester', 15000, 50, '7683b7e80fd821badc89c42001933829.jpeg'),
(10, 1, 24, 'Amstaff pulóver', 4, 'Color: yellow and black Material: 50% cotton 50% polyester', 22, NULL, 'ea137c8fe7181203d452e41501dce50a.jpeg'),
(11, 1, 24, 'Ecko Untld Pulóver', 3, 'Color: black Material: 100% polyester', 12, NULL, 'd755224a664d5980c872a9abd8d636f1.jpeg'),
(12, 1, 24, 'Sir benni pulóver', 5, 'Color: purple and grey Material: 100% polyester', 10166, NULL, '231d06bfd6980f8da9736c33fa2a6477.jpeg'),
(13, 1, 24, 'Southpole zip pulóver', 4, 'Color: white Material: 100% polyester', 12206, NULL, '69de4cc140bcac30de86d52f483109d8.jpeg'),
(14, 1, 24, 'Yakuza mikina pulóver', 2, 'Color: black Material: 100% polyester', 24500, 30, 'dae195efa07fdc09ef928488bd293685.jpeg'),
(15, 1, 25, 'Ecko Untld Szvetter', 6, 'Color: black Material: 50% polyester 50% cotton', 13355, 20, '0daf8836260705f6914f86f3bfc20eb7.jpeg'),
(16, 1, 25, 'Ecko Untld Take it EZ Szvetter', 4, 'Color: grey Material: 100% polyester', 15000, 50, 'a8131f64f7b5eb6f0e06168dded32017.jpeg'),
(17, 1, 25, 'Sir benni szvetter', 7, 'Color: grey Material: 60% polyester 40% cotton', 8000, 10, 'b176e7b58929f2e362a15c9fda8e9335.jpeg'),
(18, 1, 25, 'Sir benni fekete szvetter', 3, 'Color: black Material: 60% polyester 40% cotton', 6700, 15, 'eadbed3bfbdab3552db33fb402a2f96f.jpeg'),
(19, 1, 26, 'Amstaff téli kabát', 2, 'Color: grey Material: 60% polyester 40% cotton', 25000, 50, 'd44cc871cbe8829015c049e6fa22a24b.jpeg'),
(20, 1, 26, 'Blend winterjacket', 4, 'Color: dark grey Material: 60% polyester 40% cotton', 23500, 30, '23c74fbda33779dbe98e9d8c75d4771b.jpeg'),
(21, 1, 26, 'Elade winter jacket', 2, 'Color: black Material: 60% polyester 40% cotton', 24500, NULL, '3682dc29eed69d70d475662d50397658.jpeg'),
(22, 1, 26, 'Patria mardini winter jacet', 10, 'Color: brown Material: 60% polyester 40% cotton', 30000, 50, 'c34df39dfe95d15ebb5f3325e8ddd07e.jpeg'),
(23, 1, 26, 'Sir benni winter jacket', 5, 'Color: black Material: 50% polyester 50% cotton', 34000, 10, '8b29b947facf91bad74999e938bed49e.jpeg'),
(24, 1, 27, 'Urban classic biker leather jacket', 2, 'Urban Classics\r\n100% Original Guaranteed\r\nHigh Comfort\r\nImported', 27000, NULL, '0d2ad22cb798e7661520c86040dd324c.jpeg'),
(25, 1, 27, 'Urban classic leather jacket', 1, 'Urban Classics\r\n100% Original Guaranteed\r\nHigh Comfort\r\nImported', 27500, NULL, '2f3e9052ca8493624b07c761ac61b77a.jpeg'),
(26, 1, 27, 'Urban classic műbőr kabát', 1, 'Urban Classics\r\n100% Original Guaranteed\r\nHigh Comfort\r\nImported', 19900, NULL, 'b2f6e614d9e9647f1dbf6e39fb121953.jpeg'),
(27, 1, 16, 'Slim fit pascussi farmer', 3, 'Slim fit', 12206, NULL, '3744c9169c715ecb44655246bdd7231f.jpeg'),
(28, 1, 16, 'Thug life farmer', 2, 'Blue bootcut jeans', 13355, 0, '23d977f6c951852da9ad7601ebf1b19a.jpeg'),
(29, 1, 16, 'Tom tailor farmer', 3, 'Slim fit Tom tailor jeans', 7930, NULL, '8c437601804ec740572d6b58a4fb8c7f.jpeg'),
(30, 1, 16, 'Zoo york farmer', 2, 'Baggy black zoo york jeans', 8950, NULL, '456bd6ce35746b2b1f3ca7f6b550bda8.jpeg'),
(31, 1, 18, 'Amstaff melegítő', 3, 'Amstaff sweatpants\r\n100% Original Guaranteed\r\nHigh Comfort\r\nImported', 5000, 20, '0fa3814df09da817c7451d4f8e585e23.jpeg'),
(32, 1, 18, 'Amstaff fekete melegítő', 4, 'Colour: black', 8000, 20, '4ac71378b35f667272ab7f480a4a438b.jpeg'),
(33, 1, 18, 'Amstaff Blade Pants Black Silver', 2, 'Amstaff Blade Pants Black Silver', 12206, NULL, '203546fb882c38335e62fde47be0d90d.jpeg'),
(34, 1, 18, 'Ecko Untld melegítő', 5, 'High quality sweatpants', 9899, 10, '3c4bbf3592b7ecebd7a3d2b1d7595860.jpeg'),
(35, 1, 19, 'Ecko unlimited rövidnadrág', 25, 'Ecko unlimited shorts.', 5000, NULL, '2a226105a8413de0715cb9f41352da65.jpeg'),
(36, 1, 19, 'Ecko rövidnadrág', 4, 'High quality. \r\nBuy it.', 6700, NULL, '35f559f7e1ad1db46191d03fcca4a814.jpeg'),
(37, 1, 19, 'Ecko untld graffiti rövidnadrág', 5, 'It has a cool graffity on it.\r\nPls buy it.', 4500, NULL, '920b8fc282f385d6ad173c7143f61b1b.jpeg'),
(38, 1, 28, 'DC cipő', 6, 'These DC shoes are very hip. You''ll be the coolest dude wearing it.', 6000, NULL, '5a99235428dbb83c6ed4a4ed3ea3d117.jpeg'),
(39, 1, 28, 'Element cipők', 4, 'Element shoes. You''ll like ''em.', 7000, 15, '01ba54c7b373268e3a06249f474fcd66.jpeg'),
(40, 1, 28, 'Kappa deszkás cipő', 5, 'High quality shoes.', 12000, 20, '78626d65b44da312323fb1c12d629b8b.jpeg'),
(41, 1, 29, 'Bakancs', 4, 'Bustagrip winter shoes.', 4500, 20, 'bca76c9cdf0b0bd16fdbf604622a7bf6.jpeg'),
(42, 1, 31, 'Amstaff női rövidujjú póló', 10, 'Cool amstaff t-shirt.', 5000, 15, '0d38a8f07f58aaff312ee9b839c7a108.jpeg'),
(43, 1, 31, 'Amstaff női rövidujjú póló fehér', 13, 'White cool amstaff t-shirt.', 8000, 40, '65507a9e5457f16e071ebb99ce239be2.jpeg'),
(44, 1, 31, 'Amstaff női rövidujjú póló fekete mintás', 30, 'Black amstaff female t-shirt', 15000, 30, '12e417cf2bafaff64908e29cfa345616.jpeg'),
(45, 1, 32, 'Urban classic női póló', 15, 'High quality urban classic female long sleeve shirt', 5000, 40, '130be4023164a07d80892953374da0ce.jpeg'),
(46, 1, 33, 'Amstaff női felső', 15, 'High quality amstaff female hoodie', 15000, 30, '692d89d8fb544d625eb3630e01fcb767.jpeg'),
(47, 1, 34, 'Reebok pulóver', 30, 'High quality jumper', 3000, NULL, 'aba3e4936a64ceadb99b048114cf7132.jpeg'),
(48, 1, 33, 'Element női pulóver', 10, 'High quality element hoodie', 9899, NULL, '79267ebf514dc99eb37cbd3dbe6b96c9.jpeg'),
(49, 1, 33, 'Babystaff mikina női pulóver', 15, 'High quality', 15000, NULL, 'df9a4e9445815e96ca6380d55e0a3ae9.jpeg'),
(50, 1, 35, 'Amstaff női téli kabát', 9, 'Color: black', 15000, NULL, 'a5c0b9d08ce15c09d1777939a8adb404.jpeg'),
(51, 1, 35, 'Billabong női téli kabát', 7, 'Women''s winter jacket.', 30000, 10, 'de0ef183246845bb010c43462b087ac9.jpeg'),
(52, 1, 36, 'Urban classic biker leather jacket női', 5, 'Műbőr', 30000, NULL, '37149560e59a12b9f54ee52048c5ae60.jpeg'),
(53, 1, 36, 'Urban classic leather jacket női', 10, 'High quality', 40000, 10, 'e296679c3bc2b91abc697af72ee2a5b9.jpeg'),
(54, 1, 21, 'Billabong női rövidnadrág', 15, 'Summer shorts', 6700, NULL, '2751f0a97b16e07c5d35d40eac4b7c74.jpeg'),
(55, 1, 21, 'Amstaff női rövid nadrág', 14, 'Summer amstaff shorts', 5500, NULL, '52305c5391e9e318650e2a8afaaaaf8b.jpeg'),
(56, 1, 20, 'Endorfina leggings', 10, 'Cool leggings', 7930, NULL, '2927cc231b236d6ea0baf33d4fcb11b0.jpeg'),
(57, 1, 20, 'Amstaff leggings', 20, 'Get the leggings from a good brand.', 15000, NULL, '15826aec5fda28225bd322c501359e5a.jpeg'),
(58, 1, 17, 'Just rhyse női farmer', 12, 'Skinny jeans for women', 19900, NULL, 'b8530af3e2fef8f7b1131e217fc65a3e.jpeg'),
(59, 1, 17, 'Urban classic női farmer', 13, 'Skinny jeans for women', 18900, NULL, '105cea454612f6151fe353c59b4fbc32.jpeg'),
(60, 1, 17, 'Urban classic skinny famer női', 16, 'Skinny jeans for women', 19990, NULL, '68299618df5c9ec8bc5d1da9c9b54a39.jpeg'),
(61, 1, 37, 'Nike női cipő', 15, 'Nike shoes for women', 25000, 30, '2f60ecac3f7488c2cabcf245d2243112.jpeg'),
(62, 1, 37, 'Nike 6.0 női cipő', 10, 'Nike 6.0 shoes for women', 23400, NULL, 'de11d2c7e9734cbae1c3fbf4144251a8.jpeg'),
(63, 1, 37, 'Reebok női cipő', 5, 'Reebok shoes for women', 26000, NULL, 'a6674f3d65afb8077d7da6ed9c73736d.jpeg'),
(64, 1, 38, 'Nike air téli cipő', 9, 'Nike air winter boots for women', 31000, NULL, '415d75b87b2f1de92f3915cbbabbacb7.jpeg'),
(65, 1, 38, 'Nike air barna bakancs', 12, 'Brown nike boots for women', 32000, NULL, '313f8ca832df279fde44763a67a26814.jpeg'),
(66, 1, 4, 'Alpha industries velcro cap black', 20, 'Black hat', 3000, NULL, '5da166abcd7a47b6d606638c470502ff.jpeg'),
(67, 1, 4, 'Amstaff kesztyű', 15, 'Amstaff winter gloves', 5000, NULL, '4543ae08e37290842e8f2553bf000643.jpeg'),
(68, 1, 4, 'G-Shock karóra', 3, 'Casio G-Shock watch', 40000, 20, 'a1425e22cfb696be06365e171cb8f5c1.jpeg'),
(69, 1, 4, 'Vans napszemüveg', 15, 'Vans cool summer glasses', 15, 50, 'ac76fb930f112f0536b77cb23393a386.jpeg');
