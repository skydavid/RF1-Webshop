import $ from "jquery";

var quantityHandler = {
    init : function () {

        $('.js-cartadd > input').bind('keyup mouseup', function () {
            var href = $(this).siblings('a').attr('href');
            href = href.split('/');
            href.pop();
            href = href.join('/')+'/'+$(this).val();
            $(this).siblings('a').attr('href',href)
        })


        $('.js-cartdelete > input').bind('keyup mouseup', function () {
            var href = $(this).siblings('.js-cartdelete__stack').attr('href');
            href = href.split('/');
            href.pop();
            href = href.join('/')+'/'+$(this).val();
            $(this).siblings('.js-cartdelete__stack').attr('href',href);
        })
    }
}

export default quantityHandler;
