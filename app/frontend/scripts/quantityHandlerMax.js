import $ from "jquery";

var quantityHandlerMax = {
    init : function () {

        $('.js-cartadd-max > .inputhold > .input-number').bind('keyup mouseup', function () {
            var href = $('.to-cart').attr('href');
            href = href.split('/');
            href.pop();
            href = href.join('/')+'/'+$(this).val();
            $('.to-cart').attr('href',href)
        })
    }
}

export default quantityHandlerMax;
