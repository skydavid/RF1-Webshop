import React from 'react';
import ReactDOM from 'react-dom';
import mainSiteMount from './mainSite/root.jsx';
import injectTapEventPlugin from 'react-tap-event-plugin';
import $ from "jquery";

import quantityHandler from './quantityHandler';


$(document).ready(function() {
    $('select').material_select();
});


quantityHandler.init();
