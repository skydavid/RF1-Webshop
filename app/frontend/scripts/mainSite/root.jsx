import React from 'react';
import { render } from 'react-dom'
import App from './containers/App.jsx'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const Root = () => (
  <MuiThemeProvider>
    <App />
  </MuiThemeProvider>
);

export default function mountRootComp(props) {
    render(<Root />, document.getElementById('app'));
}
