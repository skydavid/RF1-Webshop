import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 370,
    overflowY: 'auto',
  },
};

const tilesData = [
  {
    img: 'https://s-media-cache-ak0.pinimg.com/564x/9c/ed/70/9ced705fe4762950fb01e97954592b8e.jpg',
    title: 'Sapka',
    price: '5000 Ft',
  },
  {
    img: 'http://blog.szabohutogep.hu/wp-content/uploads/2015/12/36088_csabai_kolb__sz_05_s_large_normal.jpg',
    title: 'Kolbász',
    price: '50.000 Ft',
  },
  {
    img: 'http://nemkutya.com/wp-content/uploads/2013/09/1379772923-sapka.jpg',
    title: 'Sapkaasdas',
    price: '5000 Ft',
  },
  {
    img: 'https://upload.wikimedia.org/wikipedia/commons/6/68/Csabai_kolb%C3%A1sz.jpg',
    title: 'Kolbddsz',
    price: '50.000 Ft',
  },
];

const MyAwesomeReactComponent = ({open,menuOpenEventhandler}) => {
    return (
        <div style={styles.root}>
            <GridList
                cellHeight={180}
                style={styles.gridList}
                >
                {tilesData.map((tile) => (
                    <GridTile
                        key={tile.img}
                        title={tile.title}
                        subtitle={<span><b>{tile.price}</b></span>}
                        actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
                    >
                    <img src={tile.img} />
                    </GridTile>
                ))}
            </GridList>
        </div>
    );
};

export default MyAwesomeReactComponent;
