<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;

class ProductController extends Controller
{
	/**
	* @Route("/products", name="admin.products.homepage")
	* @Template("admin/product/index.html.twig")
	*/
	public function indexAction()
	{
	    $em = $this->getDoctrine()->getManager();
	    $products = $em->getRepository('AppBundle:Product')->findAll();
			return array('products' => $products);
	}

	/**
	* @Route("/products/edit/{id}", name="admin.products.edit")
	* @Route("/products/add", name="admin.products.add")
	* @Template("admin/product/add.html.twig")
	*/
	public function addAction(Request $request, Product $product = null)
	{
		$em = $this->getDoctrine()->getManager();
		if ($product === null) {
			$product = new Product();
			$originalProductImage = null;
		}else{
			$originalProduct = $em->getRepository('AppBundle:Product')->find($product->getId());
			$originalProductImage = $originalProduct->getImage();
		}

		$form = $this->createForm(ProductType::class, $product);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$product->setOwnerUser($this->getUser());
				if($product->getImage() !== null){
					$file = $product->getImage();
	        		$fileName = md5(uniqid()).'.'.$file->guessExtension();
					$file->move(
					$this->container->getParameter('product_image_dir'),
						$fileName
	          		);
					$product->setImage($fileName);
				}else{
					$product->setImage($originalProductImage);
				}
				$em = $this->getDoctrine()->getManager();
				$em->persist($product);
				$em->flush();
			} catch (UniqueConstraintViolationException $e){
				throw new HttpFoundationExtension();
			}
			return $this->redirectToRoute('admin.products.homepage');
		}
		$form = $form->createView();
		return compact('form');
	}
	/**
	* @Route("/products/delete/{id}", name="admin.products.delete")
	*/
	public function deleteAction($id)
	{

    $em = $this->getDoctrine()->getManager();
    $product = $em->getRepository('AppBundle:Product')->find($id);
    $em->remove($product);
    $em->flush();

		return $this->redirectToRoute('admin.products.homepage');
	}
}
