<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Orders;

class OrdersController extends Controller
{
	/**
	* @Route("/orders", name="admin.orders.homepage")
	* @Template("admin/orders/orders.html.twig")
	*/
	public function indexAction(Request $request)
	{
        $em = $this->getDoctrine()->getManager();
        $orders = $em->getRepository('AppBundle:Orders')->findAll();

        return [
			'orders' => $orders
		];
	}
}