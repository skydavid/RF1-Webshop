<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;

class CategoryController extends Controller
{
	/**
	* @Route("/categories", name="admin.categories.homepage")
	* @Template("admin/category/index.html.twig")
	*/
	public function indexAction()
	{
	    $em = $this->getDoctrine()->getManager();
	    $categories = $em->getRepository('AppBundle:Category')->findAll();
			return array('categories' => $categories, 'category_image_web_path' => $this->container->getParameter('category_image_web_path'));
	}

	/**
	* @Route("/categories/edit/{id}", name="admin.categories.edit")
	* @Route("/categories/add", name="admin.categories.add")
	* @Template("admin/category/add.html.twig")
	*/
	public function addAction(Request $request, Category $category = null)
	{
		$em = $this->getDoctrine()->getManager();
		if ($category === null) {
			$category = new Category();
			$originalCategoryImage = null;
		}else{
			$originalCategory = $em->getRepository('AppBundle:Category')->find($category->getId());
			$originalCategoryImage = $originalCategory->getImage();
		}

		$form = $this->createForm(CategoryType::class, $category);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			try {
				if($category->getParentCategory() !== null){
					$category->setParentCategory($category->getParentCategory()->getId());
				}
				if($category->getImage() !== null){
					$file = $category->getImage();
	        		$fileName = md5(uniqid()).'.'.$file->guessExtension();
					$file->move(
						$this->container->getParameter('category_image_dir'),
						$fileName
					);
					$category->setImage($fileName);
				}else{
					$category->setImage($originalCategoryImage);
				}
				$em = $this->getDoctrine()->getManager();
				$em->persist($category);
				$em->flush();
			} catch (UniqueConstraintViolationException $e){
				throw new HttpFoundationExtension();
			}
			return $this->redirectToRoute('admin.categories.homepage');
		}
		$form = $form->createView();
		return compact('form');
	}
	/**
	* @Route("/categories/delete/{id}", name="admin.categories.delete")
	*/
	public function deleteAction($id)
	{

    $em = $this->getDoctrine()->getManager();
    $category = $em->getRepository('AppBundle:Category')->find($id);
    $em->remove($category);
    $em->flush();

		return $this->redirectToRoute('admin.categories.homepage');
	}
}
