<?php
namespace AppBundle\Form;

use AppBundle\Form\UserType;
use Symfony\Component\Form\FormBuilderInterface;

class UserEditType extends UserType{

	    public function buildForm(FormBuilderInterface $builder, array $options)
	    {
	        $this->setPasswordRequired(false);
	        parent::buildForm($builder, $options);
			}
}
