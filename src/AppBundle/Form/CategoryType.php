<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Category;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$currentCategoryId = $options['data']->getId();
		$currentParentCategoryId = $options['data']->getParentCategory();
		$builder
		->add('name', TextType::class, array(
				'label' => 'Name',
		))
		->add('image', FileType::class, array(
				'label' => 'Image',
				'required'    => false,
				'data_class' => null,
		))
		->add('parentCategory', EntityType::class, array(
				'label' => 'Parent category',
			'class' => 'AppBundle:Category',
			'empty_data'  => null,
			'required'    => false,
			'placeholder' => 'Parent category',
			'query_builder' => function(EntityRepository $er) use ($currentCategoryId) {
						$form = $er->createQueryBuilder('categories');
						if($currentCategoryId !== null){
							$form->where('categories.id != :id')->setParameter('id', $currentCategoryId);
						}
						$form->orderBy('categories.name', 'ASC');
				return $form;
			},
			'choice_label' => 'name',
		))
		->add('save', SubmitType::class, array(
				'label' => 'Mentés',
		));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Category',
        ));
    }
}
