<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Category;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
						->add('name', TextType::class, array(
								'label' => 'Name',
						))
            ->add('quantity', NumberType::class, array(
								'label' => 'Quantity',
						))
            ->add('description', TextareaType::class, array(
								'label' => 'Description',
								'required'    => false,
						))
            ->add('price', NumberType::class, array(
								'label' => 'Price',
						))
            ->add('discount', NumberType::class, array(
								'label' => 'Discount',
								'required'    => false,
						))
            ->add('image', FileType::class, array(
								'label' => 'Image',
								'required'    => false,
								'data_class' => null,
						))
						->add('category', EntityType::class, array(
								'label' => 'Category',
						    'class' => 'AppBundle:Category',
						    'required'    => true,
						    'query_builder' => function(EntityRepository $er) {
										$form = $er->createQueryBuilder('categories');
										$form->orderBy('categories.name', 'ASC');
						        return $form;
						    },
    						'choice_label' => 'name',
						))
            ->add('ownerUser', HiddenType::class, array(
              'data' => 0,
            ))
						->add('save', SubmitType::class, array(
								'label' => 'Mentés',
						));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product',
        ));
    }
}
