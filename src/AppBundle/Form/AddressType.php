<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('zipCode', NumberType::class)
            ->add('country', TextType::class)
            ->add('city', TextType::class)
            ->add('state', TextType::class)
            ->add('street', TextType::class)
            ->add('streetNumber', NumberType::class)
            ->add('floor', NumberType::class, array(
              'required' => false, ))
            ->add('door', NumberType::class, array(
              'required' => false, ))
            ->add('user', HiddenType::class, array(
              'data' => 0,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address',
        ));
    }
}
