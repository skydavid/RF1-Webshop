<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $addressIds = 2;

        $builder
        ->add('shippingmethod', ChoiceType::class, array(
            'label' => 'Átvételi mód',
            'choices' => [
                'Személyes átvétel' => 'Személyes átvétel',
                'Postázás' => 'Postázás'
            ],
            'required'    => false,
            'placeholder' => 'Válasszon...'
        ))
        ->add('paymentmethod', ChoiceType::class, array(
            'label' => 'Fizetési mód',
            'choices' => [
                'Előre utalás' => 'Előre utalás',
                'Utánvétel' => 'Utánvétel'
            ],
            'required'    => false,
            'placeholder' => 'Válasszon...'
        ))
        ->add('billingaddress', EntityType::class, array(
            'label' => 'Számlázási cím',
            'class' => 'AppBundle:Address',
            'required'    => false,
            'query_builder' => function(EntityRepository $er) {
                $form = $er->createQueryBuilder('address');
                $form->orderBy('address.city', 'ASC');
                return $form;
            },
            'choice_label' => function ($form) {
                return $form->getState().', '.$form->getCity().', '.$form->getStreet().' '.$form->getStreetNumber();
            },
            'placeholder' => 'Válasszon...',
        ))
        ->add('shippingaddress', EntityType::class, array(
            'label' => 'Szállítási cím',
            'class' => 'AppBundle:Address',
            'required'    => false,
            'query_builder' => function(EntityRepository $er) {
                $form = $er->createQueryBuilder('address');
                $form->orderBy('address.city', 'ASC');
                return $form;
            },
            'choice_label' => function ($form) {
                return $form->getState().', '.$form->getCity().', '.$form->getStreet().' '.$form->getStreetNumber();
            },
            'placeholder' => 'Válasszon...',
        ))
        ->add('comment', TextareaType::class, array(
				'label' => 'Megjegyzés:',
                'required'    => false,
		))
        ->add('save', SubmitType::class, array(
				'label' => 'Megrendelem',
		));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Orders',
        ));
    }
}
