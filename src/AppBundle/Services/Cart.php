<?php 
namespace AppBundle\Services;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Cart
{
    protected $em;
    protected $tokenStorage;
    protected $session;
    protected $authorizationChecker;

    public function __construct(
        EntityManager $em,
        TokenStorage $tokenStorage,
        Session $session,
        AuthorizationChecker $authorizationChecker
    )
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function fillUserCart()
    {   
        if($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {    
            return $this->fillFromDB();
        } else {
            return $this->fillFromSession();
        }    
    }

    private function fillFromSession() {
        $cartContent =  $this->session->get('cartcontent');
        return unserialize($cartContent);
    }

    private function fillFromDB() {
        return unserialize($this->tokenStorage->getToken()->getUser()->getCart());
    }


    public function addCart($cartcontent, $id, $count, $product) {
        $item = NULL;
        $count = intval($count);
        $id = intval($id);

        $cartcontent = (!$cartcontent) ? [] : $cartcontent;

        foreach ($cartcontent as $key => $value) {
            if ( $cartcontent[$key]['id'] === $id ) {
                $item = $key;
            }
        }

        if ($item === NULL) {

            if ($product->getQuantity() < $count) {
                return false;
            }

            $cartcontent[] = [
                'id' => $id,
                'name' => $product->getName(),
                'count' => $count,
                'price' =>  $product->getResultOfDiscount(),
                'image' => $product->getImage(),
            ];

        } else {

            if ($product->getQuantity() < $cartcontent[$item]['count'] + $count) {
                return false;
            }

            $cartcontent[$item]['count'] = $cartcontent[$item]['count'] + $count;
        }

        return $cartcontent;
    }

    public function deleteCart($cartcontent, $id, $count) {
        $item = NULL;
        $count = intval($count);
        $id = intval($id);

        foreach ($cartcontent as $key => $value) {
            if ( $cartcontent[$key]['id'] === $id ) {
                $item = $key;
            }
        }

        if ($item === NULL) {
            return $this->redirect($request->headers->get('referer'));
        }

        if ( $count === 0) {
            unset($cartcontent[$item]);
        } else {
            $cartcontent[$item]['count'] -= $count;
            if($cartcontent[$item]['count'] <= 0) {
                unset($cartcontent[$item]);
            }
        }

        return $cartcontent;
    }
}


?>