<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\User;

/**
* Class Product
*
* @package AppBundle\Entity
*
* @ORM\Entity()
* @ORM\Table(name="products")
* @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
*/
class Product
{
  	/**
  	* @ORM\Id
  	* @ORM\Column(type="integer")
  	* @ORM\GeneratedValue(strategy="AUTO")
  	*/
  	private $id;


    /**
    * @ORM\Column(type="string", length=255)
  	* @Assert\NotBlank()
    */
    private $name;

    /**
  	* @ORM\Column(type="integer")
    * @Assert\NotNull()
    */
    private $quantity;

    /**
    * @ORM\Column(type="text", nullable=true)
    */
    private $description;

	/**
    * @Assert\NotNull()
    * @ORM\Column(type="integer")
    * @var integer
    */
    private $price;

	/**
    * @ORM\Column(type="integer", nullable=true)
    * @var integer
    */
    private $discount;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $image;

	/**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="users")
    * @ORM\JoinColumn(name="owner_user", referencedColumnName="id")
    * @Assert\NotNull()
    */
    private $ownerUser;

	/**
    * @ORM\ManyToOne(targetEntity="Category", inversedBy="categories")
    * @ORM\JoinColumn(name="category", referencedColumnName="id")
    * @Assert\NotNull()
    */
    private $category;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param mixed name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Quantity
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the value of Quantity
     *
     * @param mixed quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param mixed description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Image
     *
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of Image
     *
     * @param mixed image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }


    /**
     * Get the value of Owner User
     *
     * @return mixed
     */
    public function getOwnerUser()
    {
        return $this->ownerUser;
    }

    /**
     * Set the value of Owner User
     *
     * @param mixed ownerUser
     *
     * @return self
     */
    public function setOwnerUser($ownerUser)
    {
        $this->ownerUser = $ownerUser;

        return $this;
    }

    /**
     * Get the value of Price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of Price
     *
     * @param integer price
     *
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of Discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set the value of Discount
     *
     * @param integer discount
     *
     * @return self
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }


    /**
     * Get the value of Category
     *
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of Category
     *
     * @param mixed category
     *
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    public function getResultOfDiscount()
    {
        return round($this->price - ($this->price * ($this->discount / 100)));
    }

}
