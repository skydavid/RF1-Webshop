<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Orders
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotNull(message="Ez a mező nem lehet üres!")
     * @ORM\Column(name="shippingmethod", type="string")
     */
    private $shippingmethod;

    /**
     * @var string
     *
     * @Assert\NotNull(message="Ez a mező nem lehet üres!")
     * @ORM\Column(name="paymentmethod", type="string")
     */
    private $paymentmethod;

    /**
     * @var string
     *
     * @ORM\Column(name="totalprice", type="decimal", precision=10, scale=0)
     */
    private $totalprice;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @Assert\NotNull(message="Ez a mező nem lehet üres!")
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="addresses")
     * @ORM\JoinColumn(name="billingaddress", referencedColumnName="id")
     */
    private $billingaddress;

    /**
     * @Assert\NotNull(message="Ez a mező nem lehet üres!")
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="addresses")
     * @ORM\JoinColumn(name="shippingaddress", referencedColumnName="id")
     */
    private $shippingaddress;

    /**
     * @var text
     * @ORM\Column(name="payload", type="text")
     */
    private $payload;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="users")
    * @ORM\JoinColumn(name="owner_user", referencedColumnName="id")
    */
    private $ownerUser;

    public function __construct()
	{
		$this->date = new \DateTime();
		$this->status = 0;
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shippingmethod
     *
     * @param string $shippingmethod
     *
     * @return Orders
     */
    public function setShippingmethod($shippingmethod)
    {
        $this->shippingmethod = $shippingmethod;

        return $this;
    }

    /**
     * Get shippingmethod
     *
     * @return string
     */
    public function getShippingmethod()
    {
        return $this->shippingmethod;
    }

    /**
     * Set paymentmethod
     *
     * @param string $paymentmethod
     *
     * @return Orders
     */
    public function setPaymentmethod($paymentmethod)
    {
        $this->paymentmethod = $paymentmethod;

        return $this;
    }

    /**
     * Get paymentmethod
     *
     * @return string
     */
    public function getPaymentmethod()
    {
        return $this->paymentmethod;
    }

    /**
     * Set totalprice
     *
     * @param string $totalprice
     *
     * @return Orders
     */
    public function setTotalprice($totalprice)
    {
        $this->totalprice = $totalprice;

        return $this;
    }

    /**
     * Get totalprice
     *
     * @return string
     */
    public function getTotalprice()
    {
        return $this->totalprice;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Orders
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Orders
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Orders
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set billingaddress
     *
     * @param integer $billingaddress
     *
     * @return Orders
     */
    public function setBillingaddress($billingaddress)
    {
        $this->billingaddress = $billingaddress;

        return $this;
    }

    /**
     * Get billingaddress
     *
     * @return int
     */
    public function getBillingaddress()
    {
        return $this->billingaddress;
    }
        
    /**
     * Set shippingaddress
     *
     * @param integer $shippingaddress
     *
     * @return Orders
     */
    public function setShippingaddress($shippingaddress)
    {
        $this->shippingaddress = $shippingaddress;

        return $this;
    }

    /**
     * Get shippingaddress
     *
     * @return int
     */
    public function getShippingaddress()
    {
        return $this->shippingaddress;
    }

    /**
     * Set payload
     *
     * @param integer $payload
     *
     * @return Orders
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Get payload
     *
     * @return text
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Get the value of Owner User
     *
     * @return mixed
     */
    public function getOwnerUser()
    {
        return $this->ownerUser;
    }

    /**
     * Set the value of Owner User
     *
     * @param mixed ownerUser
     *
     * @return self
     */
    public function setOwnerUser($ownerUser)
    {
        $this->ownerUser = $ownerUser;

        return $this;
    }
}

