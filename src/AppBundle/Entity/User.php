<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
* Class User
*
* @package AppBundle\Entity
*
* @ORM\Entity()
* @ORM\Table(name="users")
* @UniqueEntity(fields="email", message="Email already taken")
*/
class User implements UserInterface
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;

	/**
	* @ORM\Column(type="string", length=255, unique=true)
	* @Assert\NotBlank()
	* @Assert\Email()
	*/
	private $email;

	/**
	* @ORM\Column(type="string", length=255, name="first_name")
	* @Assert\NotBlank()
	*/
	private $firstName;

	/**
	* @ORM\Column(type="string", length=255, name="last_name")
	* @Assert\NotBlank()
	*/
	private $lastName;

	/**
	* @Assert\Length(max=4096)
	*/
	private $plainPassword;

	/**
	* @ORM\Column(type="string", length=64)
	*/
	private $password;

	/**
	* @Assert\DateTime()
	* @ORM\Column(type="datetime", name="registration_date")
	*/
	private $registrationDate;

	/**
	* @Assert\DateTime()
	* @ORM\Column(type="datetime", name="last_login_date", nullable = true)
	*/
	private $lastLoginDate;

	/**
	* @ORM\Column(type="string", length=64, name="phone_number", nullable = true)
	*/
	private $phoneNumber;

	/**
	* @ORM\Column(type="integer")
	*/
	private $rank;

	/**
	* @ORM\Column(type="integer", name="paid_products")
	*/
	private $paidProducts;

	/**
	* @ORM\Column(type="boolean", name="status")
	* @var boolean
	*/
	private $status;

	/**
	* @ORM\Column(type="text", nullable=true)
	* @var string
	*/
	private $cart = null;

	public function __construct()
	{
		$this->registrationDate = new \DateTime();
		$this->status = true;
		$this->paidProducts = 0;
		$this->rank = 0;
		$this->cart = null;
	}

	/**
	* Get the value of Id
	*
	* @return mixed
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	* Set the value of Id
	*
	* @param mixed id
	*
	* @return self
	*/
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	* Get the value of Email
	*
	* @return mixed
	*/
	public function getEmail()
	{
		return $this->email;
	}

	/**
	* Set the value of Email
	*
	* @param mixed email
	*
	* @return self
	*/
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	* Get the value of Email
	*
	* @return mixed
	*/
	public function getUsername()
	{
		return $this->email;
	}

	/**
	* Set the value of Email
	*
	* @param mixed email
	*
	* @return self
	*/
	public function setUsername($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	* Get the value of First Name
	*
	* @return mixed
	*/
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	* Set the value of First Name
	*
	* @param mixed firstName
	*
	* @return self
	*/
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
	* Get the value of Last Name
	*
	* @return mixed
	*/
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	* Set the value of Last Name
	*
	* @param mixed lastName
	*
	* @return self
	*/
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
	* Get the value of Plain Password
	*
	* @return mixed
	*/
	public function getPlainPassword()
	{
		return $this->plainPassword;
	}

	/**
	* Set the value of Plain Password
	*
	* @param mixed plainPassword
	*
	* @return self
	*/
	public function setPlainPassword($plainPassword)
	{
		$this->plainPassword = $plainPassword;

		return $this;
	}

	/**
	* Get the value of Password
	*
	* @return mixed
	*/
	public function getPassword()
	{
		return $this->password;
	}

	/**
	* Set the value of Password
	*
	* @param mixed password
	*
	* @return self
	*/
	public function setPassword($password)
	{
		$this->password = $password;

		return $this;
	}

	/**
	* Get the value of Registration Date
	*
	* @return mixed
	*/
	public function getRegistrationDate()
	{
		return $this->registrationDate;
	}

	/**
	* Set the value of Registration Date
	*
	* @param mixed registrationDate
	*
	* @return self
	*/
	public function setRegistrationDate($registrationDate)
	{
		$this->registrationDate = $registrationDate;

		return $this;
	}

	/**
	* Get the value of Last Login Date
	*
	* @return mixed
	*/
	public function getLastLoginDate()
	{
		return $this->lastLoginDate;
	}

	/**
	* Set the value of Last Login Date
	*
	* @param mixed lastLoginDate
	*
	* @return self
	*/
	public function setLastLoginDate($lastLoginDate)
	{
		$this->lastLoginDate = $lastLoginDate;

		return $this;
	}

	/**
	* Get the value of Phone Number
	*
	* @return mixed
	*/
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}

	/**
	* Set the value of Phone Number
	*
	* @param mixed phoneNumber
	*
	* @return self
	*/
	public function setPhoneNumber($phoneNumber)
	{
		$this->phoneNumber = $phoneNumber;

		return $this;
	}

	/**
	* Get the value of Rank
	*
	* @return mixed
	*/
	public function getRank()
	{
		return $this->rank;
	}

	/**
	* Set the value of Rank
	*
	* @param mixed rank
	*
	* @return self
	*/
	public function setRank($rank)
	{
		$this->rank = $rank;

		return $this;
	}

	/**
	* Get the value of Paid Products
	*
	* @return mixed
	*/
	public function getPaidProducts()
	{
		return $this->paidProducts;
	}

	/**
	* Set the value of Paid Products
	*
	* @param mixed paidProducts
	*
	* @return self
	*/
	public function setPaidProducts($paidProducts)
	{
		$this->paidProducts = $paidProducts;

		return $this;
	}

	/**
	* Get the value of Status
	*
	* @return boolean
	*/
	public function getStatus()
	{
		return $this->status;
	}

	/**
	* Set the value of Cart
	*
	* @param string cart
	*
	* @return self
	*/
	public function setCart($cart)
	{
		$this->cart = $cart;

		return $this;
	}

		/**
	* Get the value of Status
	*
	* @return boolean
	*/
	public function getCart()
	{
		return $this->cart;
	}

	/**
	* Set the value of Status
	*
	* @param boolean status
	*
	* @return self
	*/
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}

	public function getFullName()
	{
		return $this->firstName . " ". $this->lastName;
	}

	public function getSalt()
	{
		return null;
	}

	public function eraseCredentials()
	{
		$this->plainPassword = null;
	}

	public function getRoles()
	{
		if ($this->rank > 0) {
			return array('ROLE_ADMIN');
		}
		return array('ROLE_USER');
	}

	public function isAdmin()
	{
			return $this->rank > 0;
	}
}
