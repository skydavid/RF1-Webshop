<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* Class Category
*
* @package AppBundle\Entity
*
* @ORM\Entity()
* @ORM\Table(name="categories")
* @UniqueEntity(fields="id", message="Address already exists")
* @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
*/
class Category
{
  	/**
  	* @ORM\Id
  	* @ORM\Column(type="integer")
  	* @ORM\GeneratedValue(strategy="AUTO")
  	*/
  	private $id;

    /**
    * @ORM\Column(type="string", length=255)
  	* @Assert\NotBlank()
    */
    private $name;

	  /**
	  * @ORM\Column(type="string", length=255, nullable=true)
	  */
	  private $image;

    /**
    * @ORM\Column(type="integer", name="parent_category", nullable=true)
    */
    private $parentCategory;


    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param mixed name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Image
     *
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of Image
     *
     * @param mixed image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get the value of Parent Id
     *
     * @return mixed
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * Set the value of Parent Id
     *
     * @param mixed parentId
     *
     * @return self
     */
    public function setParentCategory($parentCategory)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

}
