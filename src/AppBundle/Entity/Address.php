<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* Class Address
*
* @package AppBundle\Entity
*
* @ORM\Entity()
* @ORM\Table(name="addresses")
* @UniqueEntity(fields="id", message="Address already exists")
*/
class Address
{
  	/**
  	* @ORM\Id
  	* @ORM\Column(type="integer")
  	* @ORM\GeneratedValue(strategy="AUTO")
  	*/
  	private $id;

    /**
  	* @ORM\Column(type="integer")
    * @Assert\NotNull()
    */
    private $zipCode;

    /**
    * @ORM\Column(type="string", length=255)
  	* @Assert\NotBlank()
    */
    private $country;

    /**
    * @ORM\Column(type="string", length=255)
  	* @Assert\NotBlank()
    */
    private $city;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank()
    */
    private $state;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank()
    */
    private $street;

    /**
    * @ORM\Column(type="integer")
    * @Assert\NotNull()
    */
    private $streetNumber;

    /**
    * @ORM\Column(type="integer", nullable=true)
    */
    private $floor;

    /**
    * @ORM\Column(type="integer", nullable=true)
    */
    private $door;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="users")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    * @Assert\NotNull()
    */
    private $user;

    public function getFullAddress()
    {
        $address = $this->country . ", " . $this->state . ", " . $this->zipCode . " " . $this->city . " " . $this->street . " street " . $this->streetNumber . ". ";
        return ($this->floor !== null && $this->door !== null) ? $address . " " . $this->floor . "/" . $this->door : $address;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of User
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of User
     *
     * @param mixed user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of Zip Code
     *
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set the value of Zip Code
     *
     * @param mixed zipCode
     *
     * @return self
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get the value of Country
     *
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of Country
     *
     * @param mixed country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of City
     *
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of City
     *
     * @param mixed city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of State
     *
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the value of State
     *
     * @param mixed state
     *
     * @return self
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get the value of Street
     *
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set the value of Street
     *
     * @param mixed street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get the value of Street Number
     *
     * @return mixed
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * Set the value of Street Number
     *
     * @param mixed streetNumber
     *
     * @return self
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * Get the value of Floor
     *
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set the value of Floor
     *
     * @param mixed floor
     *
     * @return self
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get the value of Door
     *
     * @return mixed
     */
    public function getDoor()
    {
        return $this->door;
    }

    /**
     * Set the value of Door
     *
     * @param mixed door
     *
     * @return self
     */
    public function setDoor($door)
    {
        $this->door = $door;

        return $this;
    }

    private function isOwner(User $user)
    {
        return $this->user->getId() === $user->getId();
    }

    public function userHasAccess(User $user)
    {
            return $this->isOwner($user);
    }

    public function completeAddressLine() 
    {
        return $this->state.', '.$this->city.', '.$this->street.' '.$this->streetNumber;
    }
}
