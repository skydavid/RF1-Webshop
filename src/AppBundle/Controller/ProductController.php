<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Product;

class ProductController extends Controller
{
	/**
	* @Route("/products", name="products.homepage")
	* @Template("product/index.html.twig")
	*/
	public function productAction(Request $request)
	{
    $em = $this->getDoctrine()->getManager();
    $products = $em->getRepository('AppBundle:Product')->findAll();

		return array('products' => $products, 'product_image_web_path' => $this->container->getParameter('product_image_web_path'));
	}
	/**
	* @Route("/product/{id}", name="product.data")
	* @Template("product/data.html.twig")
	*/
	public function productDataAction($id)
	{
    	$em = $this->getDoctrine()->getManager();
    	$product = $em->getRepository('AppBundle:Product')->findById($id);
		if (!$product) {
       	 throw $this->createNotFoundException('A keresett termék nem található!');
    	}

		return array('product' => $product[0], 'product_image_web_path' => $this->container->getParameter('product_image_web_path'));
	}
}
