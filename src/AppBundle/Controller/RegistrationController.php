<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller
{
	/**
	* @Route("/register", name="users.registration")
	*/
	public function registerAction(Request $request)
	{
		$user = new User();
		$form = $this->createForm(UserType::class, $user, array('required' => true));
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {

			$password = $this->get('security.password_encoder')
			->encodePassword($user, $user->getPlainPassword());
			$user->setPassword($password);

			// $session = $request->getSession();
			// $user->setCart($session->get('cartcontent'));

			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();
			return $this->redirectToRoute('users.login');
		}

		return $this->render(
				'users/register.html.twig',
				array('form' => $form->createView())
			);
		}
}
