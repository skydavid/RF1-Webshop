<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template("default/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('AppBundle:Product')->getTop6DiscountProducts();

		return array('products' => $products, 'product_image_web_path' => $this->container->getParameter('product_image_web_path'));
    }

    /**
     * @Route("/contact", name="contact")
     * @Template("default/contact.html.twig")
     */
    public function contactAction(Request $request)
    {
        return [];
	}
}
