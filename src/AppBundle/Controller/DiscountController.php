<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Product;

class DiscountController extends Controller
{
	/**
	* @Route("/discount", name="discount")
	* @Template("default/discount.html.twig")
	*/
	public function indexAction(Request $request)
	{
    $em = $this->getDoctrine()->getManager();
    $products = $em->getRepository('AppBundle:Product')->findAll();

		return array('products' => $products, 'product_image_web_path' => $this->container->getParameter('product_image_web_path'));
	}
}