<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
	/**
	* @Route("/login", name="users.login")
	*/
	public function loginAction(Request $request)
	{
		$authenticationUtils = $this->get('security.authentication_utils');
		$error = $authenticationUtils->getLastAuthenticationError();
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render('users/login.html.twig', array(
			'last_username' => $lastUsername,
			'error'         => $error,
		));
	}
	/**
	* @Route("/logout", name="users.logout")
	*/
	public function logoutAction()
	{
		return $this->redirectToRoute("homepage");
	}
}
