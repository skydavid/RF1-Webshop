<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Orders;
use AppBundle\Entity\User;

class OrdersController extends Controller
{
	/**
	* @Route("/orders", name="orders")
	* @Template("default/orders.html.twig")
	*/
	public function indexAction(Request $request)
	{
        $em = $this->getDoctrine()->getManager();
        $orders = $em->getRepository('AppBundle:Orders')->findByOwnerUser($this->getUser()->getId());

        return [
			'orders' => $orders
		];
	}
}