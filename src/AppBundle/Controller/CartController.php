<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
use AppBundle\Entity\Product;
use AppBundle\Entity\Orders;
use AppBundle\Form\OrderType;

class CartController extends Controller
{
    /**
     * @Route("/cart", name="cart")
     * @Template("default/cart.html.twig")
     */
    public function indexAction(Request $request)
    {
        list($auth, $user) = $this->getAuth();
        $totalprize = 0;
        $em = $this->getDoctrine()->getManager();
        $cartcontent = $this->getCartContent($request, $auth, $user);

        $cartcontent = $this->mergingCartcontentWithProductcount($cartcontent);

        $newOrder = new Orders();
        $formOrder = $this->createForm(OrderType::class, $newOrder);
        $formOrder->handleRequest($request);

        // $modifiedCartcontent = $this->validateQuantiysInCart($cartcontent, $em);
        // var_dump($modifiedCartcontent);
        // exit;

        if ($formOrder->isSubmitted() && $formOrder->isValid()) {
            $modifiedCartcontent = $this->validateQuantiysInCart($cartcontent, $em);
            if($modifiedCartcontent !== false) {
                $user->setCart(serialize($modifiedCartcontent));
                $em->flush();
                return $this->redirect($request->headers->get('referer'));
            }

            if(!$cartcontent) {
                return $this->redirect($request->headers->get('referer'));
            }

            foreach ($cartcontent as $value) {
                $totalprize += $value['price'] * $value['count'];
            }
            $data = $formOrder->getData();
            $data->setTotalprice($totalprize);
            $data->setOwnerUser($this->getUser());
            $data->setPayload(serialize($cartcontent));
            $em->persist($data);

            $user->setCart(NULL);
            $em->merge($user);

            $this->reduceProductCount($cartcontent, $em);

            $em->flush();

            return $this->redirectToRoute('shopdone', [
                'id' => $data->getId(),
                'paymentmethod' => $data->getPaymentmethod(),
                'shippingmethod' => $data->getShippingmethod()
            ]);
        }

        return [
            'form' => $formOrder->createView(),
            'cartcontent' => $cartcontent
        ];
    }

    /**
     * @Route("/shopdone/{id}/{paymentmethod}/{shippingmethod}", name="shopdone")
     * @Template("default/shopdone.html.twig")
     */
    public function shopDoneAction(Request $request, $id, $paymentmethod, $shippingmethod)
    {
        return [
            'paymentmethod' => $paymentmethod,
            'shippingmethod' => $shippingmethod,
            'id' => $id
        ];
    }

    /**
     * @Route("/cartadd/{id}/{count}", name="cartadd")
     * @Template("default/cart.html.twig")
     */
    public function cartAddAction(Request $request, $id, $count)
    {
        $id = intval($id);

        list($cartcontent, $product, $auth, $user) = $this->prepareCartOperation($id, $request);

        $cartcontent = $this->container->get('app.cart')
            ->addCart($cartcontent, $id, $count, $product);

        if(!$cartcontent) {
            $this->addFlash(
                'error',
                'Az Ön által kiválasztott termékből, sajnos nem tudunk a kívánt mennyiséggel szolgálni jelenleg!'
            );
            return $this->redirect($request->headers->get('referer'));
        }

        $this->saveCartContent($auth, $cartcontent, $request->getSession(), $user);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/cartdelete/{id}", name="cartdelete")
     * @Route("/cartdelete/{id}/{count}", name="cartdelete")
     * @Template("default/cart.html.twig")
     */
    public function cartDeleteAction(Request $request, $id, $count = 0)
    {
        $id = intval($id);

        list($cartcontent, $product, $auth, $user) = $this->prepareCartOperation($id, $request);

        $cartcontent = $this->container->get('app.cart')
            ->deleteCart($cartcontent, $id, $count, $product);

        $this->saveCartContent($auth, $cartcontent, $request->getSession(), $user);

        return $this->redirect($request->headers->get('referer'));
    }

    private function prepareCartOperation($id, $request) {
        list($auth, $user) = $this->getAuth();

        $cartcontent = $this->getCartContent($request, $auth, $user);

        $product = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->find($id);

        if (!$product) {
            $this->addFlash(
                'error',
                'Termék nem található!'
            );
            return $this->redirect($request->headers->get('referer'));
        }

        return [$cartcontent, $product, $auth, $user];
    }

    private function saveCartContent($auth, $cartcontent, $session, $user) {
        if($auth) {
            $em = $this->getDoctrine()->getManager();
            $user->setCart(serialize($cartcontent));
            $em->flush();
        } else {
            $session->set('cartcontent',serialize($cartcontent));
        }
    }

    private function getCartContent($request, $auth, $user) {
        if($auth){
            $cartcontent = unserialize($user->getCart());

        } else {
            $session = $request->getSession();
            $cartcontent = unserialize($session->get('cartcontent'));
        }

        return $cartcontent;
    }

    private function getAuth() {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return [$auth, $user];
    }

    private function mergingCartcontentWithProductcount($cartcontent) {
        if(!$cartcontent) {
            return NULL;
        }

        $ids = [];
        foreach ($cartcontent as $key => $value) {
            $ids[] = $value['id'];
        }
        $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->findById($ids);

        foreach ($cartcontent as $key => $value) {
            foreach ($products as $product) {
                if ( $value['id'] === $product->getId() ) {
                    $cartcontent[$key]['max'] = $product->getQuantity();
                }
            }
        }

        return $cartcontent;
    }

    // ez teljesen favágó megoldás de nem érdekes
    private function reduceProductCount($cartcontent, $em) {
        $ids = [];


        foreach($cartcontent as $value) {
            $ids[] = $value['id'];
        }

        $product = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->findById($ids);

        foreach($product as $value) {
            foreach($cartcontent as $value2) {
                if($value->getId() === $value2['id'] ) {
                    $value->setQuantity($value->getQuantity() - $value2['count']);
                }
            }
        }

        foreach($product as $value) {
            $em->merge($value);
        }

    }

    private function validateQuantiysInCart($cartcontent, $em) {
        $ids = [];
        $modifyDone = false;

        if(!$cartcontent) {
            return false;
        }

        foreach($cartcontent as $value) {
            $ids[] = $value['id'];
        }

        $product = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->findById($ids);

        foreach($product as $value) {
            foreach($cartcontent as $key2 => $value2) {
                if($value->getId() === $value2['id'] ) {
                    if($value2['count'] > $value->getQuantity()){
                        $cartcontent[$key2]['count'] = $value->getQuantity();
                        $this->addFlash(
                            'error',
                            'Módosult a kosár tartalma! '.($value2['count'] - $value->getQuantity()).' '.$value->getName().' ki lett dobva a kosárból.'
                        );
                        $modifyDone = true;
                    }

                }
            }
        }

        if($modifyDone) {
            return $cartcontent;
        }

        return false;

    }
}
