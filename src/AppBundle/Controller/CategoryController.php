<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;

class CategoryController extends Controller
{
	/**
	* @Route("/categories", name="categories.homepage")
	* @Template("category/index.html.twig")
	*/
	public function productAction(Request $request)
	{
    $em = $this->getDoctrine()->getManager();
    $categories = $em->getRepository('AppBundle:Category')->findAllParentCategory();

		return array('categories' => $categories, 'category_image_web_path' => $this->container->getParameter('category_image_web_path'));
	}
	/**
	* @Route("/categories/{id}", name="categories.productsById")
	* @Template("category/products.html.twig")
	*/
	public function productsByCategoryIdAction($id)
	{
    $em = $this->getDoctrine()->getManager();
    $currentCategory = $em->getRepository('AppBundle:Category')->find($id);
    $categories = $em->getRepository('AppBundle:Category')->findByParentCategoryId($id);

    $products = $em->getRepository('AppBundle:Product')->findByCategoryId($id);

		return array('currentCategory' => $currentCategory, 'categories' => $categories, 'products' => $products, 'product_image_web_path' => $this->container->getParameter('product_image_web_path'), 'category_image_web_path' => $this->container->getParameter('category_image_web_path'));
	}
}
