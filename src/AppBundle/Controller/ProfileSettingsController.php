<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserEditType;
use AppBundle\Form\AddressType;
use AppBundle\Entity\User;
use AppBundle\Entity\Address;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProfileSettingsController extends Controller
{
  private function checkIfUserHasAccess(User $user, Address $address)
  {
      if (!$address->userHasAccess($user)) {
          throw $this->createAccessDeniedException();
      }
  }
  /**
	* @Route("/settings", name="users.settings")
	*/
  public function settingsAction(Request $request, User $user = null)
  {
    if($user === null){
      $user = new User();
    }

    $user = $this->get("security.token_storage")->getToken()->getUser();

		$form = $this->createForm(UserEditType::class, $user);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {

      $password = $this->get('security.password_encoder')
      ->encodePassword($user, $user->getPlainPassword());
      $user->setPassword($password);

			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();
			return $this->redirectToRoute('users.settings');
		}

		return $this->render(
			'users/settings.html.twig',
			array('form' => $form->createView())
		);
	}

  /**
	* @Route("/addresses/edit/{id}", name="user.addresses.edit")
	* @Route("/addresses", name="users.addresses")
	*/
  public function addressesAction(Request $request, Address $address = null)
  {
    if($address === null){
      $address = new Address();
    }else{
	    $this->checkIfUserHasAccess($this->getUser(), $address);
		}

    $em = $this->getDoctrine()->getManager();

		$form = $this->createForm(AddressType::class, $address);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$address->setUser($this->getUser());
			$em->persist($address);
			$em->flush();
			return $this->redirectToRoute('users.addresses');
		}

    $addresses = $em->getRepository('AppBundle:Address')->findBy(array('user' => $this->getUser()->getId()));

		return $this->render(
			'users/addresses.html.twig',
			array(
        'form' => $form->createView(),
        'addresses' => $addresses
    ));
	}

  /**
  * @Route("/addresses/delete/{id}", name="users.addresses.delete")
  */
  public function deleteButtonAction($id){

    $em = $this->getDoctrine()->getManager();
    $address = $em->getRepository('AppBundle:Address')->find($id);
    echo $address->getCountry();
    $em->remove($address);
    $em->flush();

		return $this->redirectToRoute('users.addresses');
  }
}
