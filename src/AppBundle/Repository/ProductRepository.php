<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findByCategoryId($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Product p WHERE p.category = :categoryId'
            )
						->setParameter('categoryId', $id)
            ->getResult();
    }
    public function getTop6DiscountProducts($limit = 6)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Product p WHERE p.discount != 0 ORDER BY p.discount DESC'
            )->setMaxResults($limit)->getResult();
    }
}
