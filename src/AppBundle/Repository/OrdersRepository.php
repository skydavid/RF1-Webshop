<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class OrdersRepository extends EntityRepository
{
    public function findByOwnerUser($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Orders c WHERE c.ownerUser = :owneruserId'
            )
						->setParameter('owneruserId', $id)
            ->getResult();
    }
}
