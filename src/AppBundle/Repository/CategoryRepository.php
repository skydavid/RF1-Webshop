<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findAllParentCategory()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Category c WHERE c.parentCategory IS NULL'
            )
            ->getResult();
    }

    public function findByParentCategoryId($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Category c WHERE c.parentCategory = :parentId'
            )
						->setParameter('parentId', $id)
            ->getResult();
    }
}
