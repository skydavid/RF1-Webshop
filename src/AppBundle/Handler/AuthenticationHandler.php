<?php
namespace AppBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Routing\Router;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AuthenticationHandler extends DefaultAuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $container;
    private $router;
    private $session;
    private $em;
    private $tokenStorage;

    /**
    * Constructor
    */
    public function __construct(Router $router, $container,Session $session, EntityManager $em, TokenStorage $tokenStorage)
    {
        $this->container = $container;
        $this->router = $router;
        $this->em = $em;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();
				if($user){
					$user->setLastLoginDate(new \DateTime());
					$this->container->get('doctrine')->getEntityManager()->flush();
				}
    
        $user->setCart(serialize($this->mergeSessionCartToDbCart()));
        $this->em->flush();
        return new RedirectResponse($this->router->generate('homepage'));
    }

    private function mergeSessionCartToDbCart() {
        $SessionContent = unserialize($this->session->get('cartcontent'));
        $DbContent = unserialize($this->tokenStorage->getToken()->getUser()->getCart());

        $resoult = $DbContent;
        $find;
        $ids = [];

        if (!$SessionContent) {
            return $DbContent;
        }     

        foreach ($SessionContent as $key => $value) {
            $ids[] = $value['id'];
        }

        if(!empty($DbContent)){
            foreach ($DbContent as $key => $value) {
                $ids[] = $value['id'];
            }
        }else{
            return $SessionContent;
        }

        //Ez egy ArrayCollection lett
        $products = $this->em
                ->getRepository('AppBundle:Product')
                ->findById($ids);

        /*
        Azzal a feltétellel indul a ciklus hogy a Db-ben nincs meg a product,
        ezt a Session kosarában való keresgélés változtathatja meg.
        --SZÖRNYET ALKOTTAM--
        */
        foreach($SessionContent as $sValue) {
            $find = false;
            foreach($DbContent as $dValue) {
                // Itt van a találat, tehát ez kilövi azt a lehetőséget hogy a Session adott eleme mergelésre kerül.
                if($dValue['id'] === $sValue['id']){
                    $find = true;
                }
            }
            if (!$find) {
                $resoult[] = $sValue;
            } else {
                foreach($resoult as $rKey => $rValue) {
                    if($rValue['id'] === $sValue['id']) {
                        foreach($products as $product) {
                            if($product->getId() === $rValue['id']) {
                                if($rValue['count'] + $sValue['count'] > $product->getQuantity()){
                                    $resoult[$rKey]['count'] = $product->getQuantity();
                                    $this->session->getFlashBag()->add(
                                        'error',
                                        'A kosárból el lett távolítva '.($rValue['count'] + $sValue['count'] - $product->getQuantity()).' '.$product->getName().' mert meghaladta a maximális mennyiséget.'
                                    );
                                } else {
                                    $resoult[$rKey]['count'] = $rValue['count'] + $sValue['count'];
                                }
                            }
                        }
                    }
                }
            }
        }
        return $resoult;
    }
}
