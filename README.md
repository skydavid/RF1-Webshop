[![build status](https://gitlab.com/skydavid/RF1-Webshop/badges/develop/build.svg)](https://gitlab.com/skydavid/RF1-Webshop/commits/develop)


# ======= Környezeti beállítások ==========

* Először is telepítsetek valamilyen apache2 webservert, a legkönyebb :
https://www.apachefriends.org/hu/ , itt ha elinditjátok a control panelről az
apache-t és a mysql-t akkor meg is vannak a localservereitek, esetleg még annyi hogy
a htdocs mappába kell majd klónozzátok a repot.

* Valahogyan eléritek hogy a php parancs elérhető legyen console-ból,
telepíthettek egyet, én a feltelpitett xampp-ból megnéztem a php.exe elérési útvonalát,
és ugyan úgy mint JAVA-nál hozzá adtam a rendszerhez mint környezeti változót.

* Composer-t installáltok, ez fogja a php-hoz behuzni a függőségeket:
https://getcomposer.org/doc/00-intro.md#installation-windows

* Telepítetek egy Node.js-t: https://nodejs.org/en/ , ez kezeli majd a gulp-hoz
a függőségeket ami az automatikus fordítást fogja végezni nekünk.
**ACHTUNG** én "npm -v" 3.10.6 verziót használok csomag kezelésre és "node -v"
v4.4.3, törekedjetek ti is erre mert lehetnek különbségek!(A 4.x.x már jó elvileg)

* Telepititek a gulpot, nyittok egy console-t és a következő linken mindent megtaláltok:
https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md , csak az első lépés kell innen!

* Telepíteni kell még a SASS compilert, ehhez a link: http://sass-lang.com/install (igen kell a ruby-s/gem-es dolog


Én Power Shell-t használok, ami teljesen hasonloan müködik mint egy linuxos terminal.

# ======= Függőségek letöltése ==========
No, ha megyvagytok mindennel akkor el lehet kezdeni a függőségeket behúzni.

##### cd a projekt mappájába.
#
```sh
composer install
```
Ez az összes symfony-php függőséget befogja húzni, csinál 1 vendor mappát és ott
lesz minden, ez gitignore-ban van, maradjon is ott!!!. A symfonynak az installálása
fog pár kérdést feltenni hogy hol van az adatbázisotok stb, minden maradjon default,
ha csak nincs password a DB-teken, de a xamppnál nincsen. Lehet hogy hisztizni fog
hogy nem találja az adott db-t de akkor "webshoprf" néven csináljatok egyet, majd
futassátok le újra a fenti parancsot.


```sh
npm install
```    
Ez letölt minden függőséget a Gulp-nak és a ReactJS-nek. A későbbiekben ha lesz behuzva
új dolog akkor ezt ujra le kell futtatni hogy frissítse a függőségeket.

# ======= Kész vagyunk, mehetne a kódolás?! ==========
Majdnem, használnunk kell a Gulp-ot a frontendes dolgokra, mert az fordítja ki a SASS-t
tiszta css-re a web/css mappába, valamint a JSX/JS-t is csak ezt a web/js mappába, és innen
van behuzva a templatek-be.

Annyi dolgunk van amikor elkezdünk kódolni frontend(nyilván a backendhez köze a Gulpnak),
vagy mikor először röffented be egy gites frissítés után a kódot hogy lefuttatjuk a gulpot
ami a következő parancs:
```sh
gulp
```
Kifog mindent buildelni az előbb említett mappákba, ha nincs syntax error, de ezt irja is.
A következő parancsot ha beirjátok akkor nem kell folyton fordítgatnotok a gulp parancsot beirkálva,
hanem automatikusan figyeli a változásokat (ez csak a frontend mappára van kiterjesztve), és
kiforditja automatikusan, cool(Néha beszarik, akkor CTRL+C és ujra inditani). A parancsa:
```sh
gulp watch
```
Ha minden klappolt, akkor a következő linken érhetitek el:
   ##### http://localhost/projektnev/web/app_dev.php/
(A projekt nev a xampp/htdocs/projektnev mappát jelzi, nállam
http://localhost/webshoprf/web/app_dev.php/ és itt van a mappa: C:\xampp\htdocs)

**ACHTUNG!!** az app_dev.php fontos az URL-ben, minden ezután jön, az hogy ezen scripten keresztül
használjuk a symfony-t jelenti a delepoment módot és sok-sok featuret ad amivel könnyebb fejleszteni,
enélkül egyenlőre nem is müködne a dolog.

##### ReactJs-hez link:
https://facebook.github.io/react/
https://www.tutorialspoint.com/reactjs/

##### Babel/ES5/ES6:
https://medium.com/sons-of-javascript/javascript-an-introduction-to-es6-1819d0d89a0f#.2fbh7zk0o

##### Sass-hoz link:
http://sass-lang.com/documentation/file.SASS_REFERENCE.html
https://www.tutorialspoint.com/sass/

##### Symfony-hoz hasznos linkek:
https://symfony.com/doc/3.0/book/installation.html
https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
https://symfony.com/doc/3.0/book/doctrine.html
https://symfony.com/doc/3.0/book/templating.html
https://symfony.com/doc/3.0/book/security.html
https://symfony.com/doc/3.0/cookbook/email.html
https://symfony.com/doc/3.0/cookbook/logging/monolog.html
https://symfony.com/doc/3.0/bundles/SensioGeneratorBundle/index.html
