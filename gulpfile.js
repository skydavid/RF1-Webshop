/*
@Author Szpisják Viktor
@Version 1.0
Babel for React task based on http://jpsierens.com/tutorial-gulp-javascript-2015-react/
*/

var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var babelify = require('babelify');
var sass = require('gulp-ruby-sass');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var postcss = require('gulp-postcss');
var rename = require('gulp-rename');
var del = require('del');
var uglify = require('gulp-uglify');
var pump = require('pump');
var injectTapEventPlugin = require("react-tap-event-plugin");

var dependencies = [
	'jquery',
];

var scriptsCount = 0;

gulp.task('compress', function (cb) {
  pump([
        gulp.src('web/js/*.js'),
        uglify(),
        gulp.dest('web/js')
    ],
    cb
  );
});

gulp.task('scripts', function () {
    bundleApp(false);
});

gulp.task('deploy', function (){
	bundleApp(true);
});

gulp.task('watch', function () {
	gulp.watch(['./app/frontend/scripts/**/*.js'], ['scripts']);
	gulp.watch(['./app/frontend/scripts/**/*.jsx'], ['scripts']);
	gulp.watch(['./app/frontend/styles/**/*.scss'], ['styles']);
});


gulp.task('default', ['clean','scripts','styles']);

function bundleApp(isProduction) {
	scriptsCount++;
	var appBundler = browserify({
    	entries: './app/frontend/scripts/app.js',
    	debug: false
  	})
  	if (!isProduction && scriptsCount === 1){
  		browserify({
			require: dependencies,
			debug: false
		})
			.bundle()
			.on('error', gutil.log)
			.pipe(source('vendors.js'))
			.pipe(gulp.dest('./web/js/'));
  	}
  	if (!isProduction){
  		dependencies.forEach(function(dep){
  			appBundler.external(dep);
  		})
  	}

  	appBundler

	  	.transform("babelify", {presets: ["es2015", "react"]})
	    .bundle()
	    .on('error',gutil.log)
	    .pipe(source('bundle.js'))
	    .pipe(gulp.dest('./web/js/'));
}

gulp.task('clean', function() {
  return del(['web/js', 'web/css']);
});

gulp.task('styles', function() {
  return sass('app/frontend/styles/*.scss', { style: 'expanded' })
    .pipe(gulp.dest('web/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('web/css'));
});
gulp.task('devmod', function() {
    return process.env.NODE_ENV = 'development';
});

gulp.task('prodmod', function() {
    return process.env.NODE_ENV = 'production';
});

function handleError(err) {
  this.emit('end');
}
